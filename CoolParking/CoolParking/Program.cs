﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            var parkingService = new ParkingServiceProxy();

            while (true)
            {
                Console.WriteLine("Выберете опцию: I (information) | V (vehicle)");
                string firstSelection = Console.ReadLine();
                switch (firstSelection)
                {
                    case "I":
                        {
                            Console.WriteLine("Выберете опцию: \n " +
                                "B (parking balance) \n " +
                                "E (current earned) \n " +
                                "S (vacand|occupied status) \n " +
                                "T (transactions before log record) \n " +
                                "H (transaction history) \n " +
                                "V (vehicle list)");

                            string selection = Console.ReadLine();
                            switch (selection)
                            {
                                case "B":
                                    {
                                        Console.WriteLine(parkingService.GetBalance());
                                        break;
                                    }
                                case "E":
                                    {
                                        Console.WriteLine();
                                        break;
                                    }
                                case "S":
                                    {
                                        Console.WriteLine($"{parkingService.GetFreePlaces()}/{parkingService.GetCapacity()}");
                                        break;
                                    }
                                case "T":
                                    {
                                        foreach (var t in parkingService.GetLastParkingTransactions())
                                        {
                                            Console.WriteLine(t);
                                        }
                                        break;
                                    }
                                case "H":
                                    {
                                        Console.WriteLine(parkingService.ReadFromLog());
                                        break;
                                    }
                                case "V":
                                    {
                                        foreach (var v in parkingService.GetVehicles())
                                        {
                                            Console.WriteLine(v);
                                        }
                                        break;
                                    }
                                default:
                                    Console.WriteLine("Необходимо выбрать одну из операций");
                                    break;
                            }
                            break;
                        }
                    case "V":
                        {
                            Console.WriteLine("Выберите опцию: \n " +
                                "A (add vehicle) \n " +
                                "R (remove vehicle) \n " +
                                "B (replenish balance)");

                            string selection = Console.ReadLine();
                            switch (selection)
                            {
                                case "A":
                                    {
                                        Console.WriteLine("Введите номер транспортного средства \n");
                                        string id = Console.ReadLine();
                                        Console.WriteLine("Введите тип транспортного средства \n");
                                        var t = Enum.Parse<VehicleType>(Console.ReadLine());
                                        Console.WriteLine("Введите стартовый баланс транспортного средства \n");
                                        decimal balance = decimal.Parse(Console.ReadLine());
                                        var v = new Vehicle(id, t, balance);
                                        parkingService.AddVehicle(v);
                                        break;
                                    }
                                case "R":
                                    {
                                        Console.WriteLine("Введите номер транспортного средства \n");
                                        string vehicleId = Console.ReadLine();
                                        parkingService.RemoveVehicle(vehicleId);
                                        break;
                                    }
                                case "B":
                                    {
                                        Console.WriteLine("Введите номер транспортного средства \n");
                                        string vehicleId = Console.ReadLine();
                                        decimal sum = decimal.Parse(Console.ReadLine());
                                        parkingService.TopUpVehicle(vehicleId, sum);
                                        break;
                                    }
                                default:
                                    Console.WriteLine("Необходимо выбрать одну из операций");
                                    break;
                            }
                            break;
                        }
                    default:
                        Console.WriteLine("Необходимо выбрать одну из операций");
                        break;
                }
            }
        }
    }
}