﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CoolParking.WebAPI.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddParkingServiceAsSingleton(this IServiceCollection servicesCollection)
        {
            ILogService logService = new LogService("log.txt");
            var parkingService = new ParkingService(
                new TimerService(Settings.BillingPeriodSeconds),
                new TimerService(Settings.LogPeriodSeconds),
                logService);

            servicesCollection.AddSingleton(typeof(IParkingService), parkingService);
        }
    }
}
