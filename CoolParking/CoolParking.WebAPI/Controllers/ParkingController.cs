﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService) => _parkingService = parkingService;

        [HttpGet]
        public decimal Balance() => _parkingService.GetBalance();

        [HttpGet]
        public int Capacity() => _parkingService.GetCapacity();

        [HttpGet]
        public int FreePlaces() => _parkingService.GetFreePlaces();
    }
}
