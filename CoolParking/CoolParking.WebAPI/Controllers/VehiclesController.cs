﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public JsonResult Get()
        {
            var vehicles = _parkingService.GetVehicles();
            return new JsonResult(vehicles);
        }

        [Route("api/[controller]/{vehicleId?}")]
        public IActionResult Get(string vehicleId)
        {
            if (!Vehicle.IsIdCorrect(vehicleId))
            {
                return new StatusCodeResult(400);
            }

            var vehicle = _parkingService
                .GetVehicles()
                .FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
            {
                return new StatusCodeResult(404);
            }

            return new JsonResult(vehicle);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Vehicle vehicle)
        {
            if (Vehicle.IsIdCorrect(vehicle.Id))
            {
                return new StatusCodeResult(400);
            }

            _parkingService.AddVehicle(vehicle);
            return Created(string.Empty, vehicle);
        }

        [HttpDelete("{vehicleId}")]
        public StatusCodeResult Delete([FromRoute] string vehicleId)
        {
            if (!Vehicle.IsIdCorrect(vehicleId))
            {
                return new StatusCodeResult(400);
            }

            if (!_parkingService.GetVehicles().Any(v => v.Id == vehicleId))
            {
                return new StatusCodeResult(404);
            }

            _parkingService.RemoveVehicle(vehicleId);
            return new StatusCodeResult(204);
        }
    }
}
