﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public JsonResult Last()
        {
            var lastTransaction = _parkingService.GetLastParkingTransactions();
            return new JsonResult(lastTransaction);
        }


        public IActionResult All()
        {
            string log;
            try
            {
                log = _parkingService.ReadFromLog();
            }
            catch (InvalidOperationException e)
            {
                return new StatusCodeResult(404);
            }
            return Ok(log);
        }


        [HttpPut]
        public IActionResult TopUpVehicle([FromBody] TopUpVehicleModel topUpVehicleModel)
        {
            if (topUpVehicleModel.Id == default ||
                topUpVehicleModel.Sum == default ||
                !Vehicle.IsIdCorrect(topUpVehicleModel.Id))
            {
                return new StatusCodeResult(400);
            }

            var vehicle = _parkingService
                .GetVehicles()
                .FirstOrDefault(v => v.Id == topUpVehicleModel.Id);

            if (vehicle is null)
            {
                return new StatusCodeResult(404);
            }

            _parkingService.TopUpVehicle(topUpVehicleModel.Id, topUpVehicleModel.Sum);

            var updatedVehicle = _parkingService
                .GetVehicles()
                .FirstOrDefault(v => v.Id == topUpVehicleModel.Id);

            return new JsonResult(updatedVehicle);
        }
        //PUT api/transactions/topUpVehicle
        //Body schema: { “id”: string, “Sum”: decimal }
        //Body example: { “id”: “GP - 5263 - GC”, “Sum”: 100 }
        //Response:
        //If body is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
        //Body example: { “id”: “GP - 5263 - GC”, “vehicleType”: 2, "balance": 245.5 }
    }
}
