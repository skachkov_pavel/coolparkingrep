﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; private set; } = new List<Vehicle>();
        public int Capacity { get; private set; }
        
        private static Parking instance;
        private static readonly object _locker = new();

        private Parking() { }

        private Parking(decimal balance, int capacity)
        {
            Balance = balance;
            Capacity = capacity;
        }

        public static Parking Instance
        {
            get
            {
                lock (_locker)
                    if (instance == null)
                    {
                        instance = new Parking(Settings.InitialParkingBalance, Settings.Capacity);
                    }

                return instance;
            }
        }

        internal void Reset()
        {
            instance = new Parking(Settings.InitialParkingBalance, Settings.Capacity);
        }
    }
}
