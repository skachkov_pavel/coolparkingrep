﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo 
    {
        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            VehicleId = vehicleId;
            TransactionDate = DateTime.UtcNow;
        }

        public decimal Sum { get; private set; }

        public string VehicleId { get; private set; }

        public DateTime TransactionDate { get; private set; }

        public override string ToString()
        {
            return $"{nameof(VehicleId)}: {VehicleId}, {nameof(TransactionDate)}: {TransactionDate}, {nameof(Sum)}: {Sum}";
        }
    }
}