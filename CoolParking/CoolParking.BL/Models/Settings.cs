﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public const decimal InitialParkingBalance = 0;

        public const int Capacity = 10;

        public const int BillingPeriodSeconds = 5;

        public const decimal OverdraftMultiplier = 2.5M;

        public const decimal PassengerCarFee = 2;

        public const decimal TruckFee = 5;

        public const decimal BusFee = 3.5M;

        public const decimal MotorcycleFee = 1;

        public const double LogPeriodSeconds = 60;
    }
}