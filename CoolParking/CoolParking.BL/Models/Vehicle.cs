﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private const string _pattern = "[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        [Required]
        public string Id { get; set; }

        [Required]
        public VehicleType VehicleType { get; private set; }
        
        [Required]
        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            AssertIdCorrectlyFormatted(id);
            AssertPositiveBalance(balance);

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static bool IsIdCorrect(string id) => !string.IsNullOrEmpty(id) && Regex.IsMatch(id, _pattern);

        public override string ToString() => $"{nameof(Id)}: {Id}, {nameof(VehicleType)}: {VehicleType}, {nameof(Balance)}: {Balance}";

        internal decimal Charge()
        {
            decimal chargeAmmount = 0;
            var oldBalance = Balance;
            switch (VehicleType)
            {
                case VehicleType.PassengerCar:
                    {
                        chargeAmmount = Settings.PassengerCarFee;
                        break;
                    }
                case VehicleType.Truck:
                    {
                        chargeAmmount = Settings.TruckFee;
                        break;

                    }
                case VehicleType.Bus:
                    {
                        chargeAmmount = Settings.BusFee;
                        break;
                    }
                case VehicleType.Motorcycle:
                    {
                        chargeAmmount = Settings.MotorcycleFee;
                        break;
                    }
            }

            var overdraft = Balance - chargeAmmount;
            if (overdraft < 0)
            {
                Balance += Settings.OverdraftMultiplier * overdraft;
            }
            else
            {
                Balance -= chargeAmmount; 
            }

            return oldBalance - Balance;
        }

        private void AssertPositiveBalance(decimal balance)
        {
            if (balance < 0)
                throw new ArgumentException("Negative balance");
        }

        private void AssertIdCorrectlyFormatted(string id)
        {
            if (!Regex.IsMatch(id, _pattern))
                throw new ArgumentException("Bad Id format");
        }

        private static string RandomUpperCaseString(int size)
        {
            var builder = new StringBuilder(size);

            char offset = 'A';
            const int lettersOffset = 26; 
            Random random = new Random();

            for (var i = 0; i < size; i++)
            {
                var newChar = (char)random.Next(offset, offset + lettersOffset);
                builder.Append(newChar);
            }

            return builder.ToString();
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            return $"{RandomUpperCaseString(2)}-{random.Next(0,9999).ToString("D4")}-{RandomUpperCaseString(2)}";
        }
    }
}