﻿namespace CoolParking.BL.Models
{
    public class TopUpVehicleModel
    {
        public TopUpVehicleModel(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }

        public string Id { get; set; }

        public decimal Sum { get; set; }
    }
}

