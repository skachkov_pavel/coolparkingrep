﻿using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;

        public TimerService(double intervalSeconds)
        {
            _timer = new Timer();
            Interval = TimeSpan.FromSeconds(intervalSeconds).TotalMilliseconds;
            _timer.Elapsed += (s, e) => Elapsed(s, e);
        }

        public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose() => _timer.Dispose();

        public void Start() => _timer.Start();

        public void Stop() => _timer.Stop();
    }
}