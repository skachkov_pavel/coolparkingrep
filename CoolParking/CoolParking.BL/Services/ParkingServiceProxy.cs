﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingServiceProxy : IParkingService
    {
        private const string BaseUri = "http://localhost:24066";
        private readonly HttpClient _client = new();

        public decimal GetBalance()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/parking/balance");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return decimal.Parse(content);
        }

        public int GetCapacity()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/parking/capacity");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return int.Parse(content);
        }

        public int GetFreePlaces()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/parking/freePlaces");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return int.Parse(content);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/transactions/last");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<TransactionInfo[]> (content); 
        }

        public string ReadFromLog()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/transactions/all");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return content; // формат
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var uri = new Uri(new Uri(BaseUri), "/api/vehicles");
            var result = _client.GetAsync(uri).Result;
            var content = result.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(content);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var uri = new Uri(new Uri(BaseUri), $"/api/vehicles/{vehicleId}");
            _client.DeleteAsync(uri).Wait();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var uri = new Uri(new Uri(BaseUri), "/api/transactions/topUpVehicle");
            HttpContent content = new StringContent(JsonConvert.SerializeObject(new TopUpVehicleModel(vehicleId, sum)), Encoding.UTF8, "application/json");
            _client.PutAsync(uri, content).Wait();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            var uri = new Uri(new Uri(BaseUri), "/api/transactions/topUpVehicle");
            HttpContent content = new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json");
            _client.PostAsync(uri, content).Wait();
        }
    }
}