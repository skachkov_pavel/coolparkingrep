﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logFilePath;
        private readonly object _locker = new();

        public LogService(string logFilePath)
        {
            _logFilePath = logFilePath;
            File.Create(_logFilePath).Close();
        }

        public string LogPath => _logFilePath;

        public string Read()
        {
            lock (_locker)
            {
                if (!File.Exists(_logFilePath))
                    throw new InvalidOperationException();
                return File.ReadAllText(_logFilePath);
            }
        }

        public void Write(string logInfo)
        {
            lock (_locker)
            {
                using var sr = File.AppendText(_logFilePath);
                sr.WriteLine(logInfo);
            }
        }
    }
}