﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Parking _parking;
        private readonly List<TransactionInfo> _transactions = new();
        private readonly object _locker = new();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Start();
            _withdrawTimer.Elapsed += (s, e) => ChargeAllVehicles();

            _logTimer.Start();
            _logTimer.Elapsed += (s, e) => LogTransactions();

            _parking = Parking.Instance;
        }

        private void LogTransactions()
        {
            lock (_locker) 
            {
                _logService.Write($"\n");
                foreach (var t in _transactions)
                {
                    _logService.Write($"{t.TransactionDate.Day} / " +
                        $"{t.TransactionDate.Month} {t.TransactionDate.Year} / " +
                        $"{t.TransactionDate:hh:mm:ss tt}: " +
                        $"{t.Sum} " +
                        $"money withdrawn from vehicle with Id = {t.VehicleId}.\n");
                }
                _transactions.Clear();
            }
        }

        private void ChargeAllVehicles()
        {
            lock (_locker)
            {
                foreach (var v in _parking.Vehicles)
                {
                    var transaction = new TransactionInfo(v.Charge(), v.Id);
                    _parking.Balance += transaction.Sum;
                    _transactions.Add(transaction);
                }
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
                throw new ArgumentException($"The vehicle with the Id: {vehicle.Id} already exists");

            if (ParkingHasPlace())
                _parking.Vehicles.Add(vehicle);
            else
                throw new InvalidOperationException("Parking has no free places");
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!_parking.Vehicles.Any(v => v.Id == vehicleId))
                throw new ArgumentException($"The vehicle with the Id: {vehicleId} does not exist in the parking");

            _parking.Vehicles.RemoveAll(v => v.Id == vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("Negative sum");

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
                throw new ArgumentException($"The vehicle with the Id: {vehicleId} does not exist in the parking");

            vehicle.Balance += sum;
        }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.Reset();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        private bool ParkingHasPlace()
        {
            return _parking.Capacity - _parking.Vehicles.Count > 0;
        }
    }
}